const buttons = document.getElementsByClassName('archive');
for( var i=0; i < buttons.length; i++){
    buttons[i].addEventListener('click', function(e){
        const id = e.target.getAttribute('data');
        console.log(id);
        $.ajax({
            url: "/cards/archive",
            data: { id: id },
            success: function(){
                console.log('success');
                location.reload();
            }
        });
    });
}
