from django import http
from django.contrib.auth import mixins, decorators
from django.views import generic
from cards import models


class CardListView(mixins.LoginRequiredMixin, generic.ListView):
    """A list of cards associated with the current user."""
    model = models.Card

    def get_queryset(self):
        return self.model.objects.filter(user_pin__user=self.request.user).order_by('is_archived')


@decorators.login_required
def archive(request):
    """Flip the archive bit on a card."""
    card_id = int(request.GET['id'])        # TODO: this should be a PUT or POST and bone up on CSRF threats etc
                                            # and handle card doesn't exist and why is not a view like above and do you
                                            # call this an API?! do you know what a REST API even is?!? etc. :-)
    try:
        card = models.Card.objects.get(pk=card_id, user_pin__user=request.user)
        card.is_archived = not card.is_archived
        card.save()
        response = 'Win'
    except models.Card.DoesNotExist:
        response = 'Lose'
    return http.HttpResponse(response)
