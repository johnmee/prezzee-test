from django.contrib import admin

from cards import models

admin.site.register(models.UserPin)
admin.site.register(models.Product)
admin.site.register(models.Card)
