from django.contrib.auth import models as auth_models
from django.test import TestCase
from . import models


class ProductTest(TestCase):

    fixtures = ['initial_data.yaml']

    def test_product(self):
        """Test basic workings of a Product."""
        myer_name = u'Myer'
        myer_desc = u'Buy stuff at Myers'
        myer_product = models.Product.objects.create(
            name=myer_name,
            description=myer_desc)
        self.assertEqual(myer_name, myer_product.name)
        self.assertEqual(myer_desc, myer_product.description)

    def test_fixtures(self):
        """Check the fixtures loaded ok."""
        self.assertEqual(1, models.Product.objects.filter(name=u'MyerCard').count())

    def test_visual_inspection(self):
        """Print out the initial data."""
        for product in models.Product.objects.all():
            print(product.name)

        for user in models.UserPin.objects.all():
            print(user.pin)

        print(auth_models.User.objects.all())

        print(models.Card.objects.all())
