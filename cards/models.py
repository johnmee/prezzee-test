from django.db import models
from django.contrib.auth import models as auth_models


class UserPin(models.Model):
    """A user with a Personally Identifying Number (code), like a password."""
    user = models.OneToOneField(auth_models.User, on_delete=models.CASCADE)
    pin = models.CharField(max_length=50)

    def __str__(self):
        return self.user.username


class Product(models.Model):
    """A product.  The branding on a card seems a likely guess."""
    name = models.CharField(max_length=200)
    description = models.TextField()

    def __str__(self):
        return self.name


class Card(models.Model):
    """A Gift Card.  Effectively a temporal debit card."""
    user_pin = models.ForeignKey(UserPin, on_delete=models.PROTECT)
    voucher_number = models.CharField(max_length=50)                 # TODO: should voucher+product be unique?
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    aud_value = models.DecimalField(max_digits=8, decimal_places=2)  # TODO: Consider a Money library
    is_archived = models.BooleanField(default=False)
