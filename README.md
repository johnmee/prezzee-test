# Prezzee interview gift card exercise.

### Coding Exercise: Gift Card Wallet

Build a web-based gift card wallet app using Python 3 and the Django 2 web framework.

### Implementation

The application should be configured with the standard ​ django auth framework​ , which includes
a standard User model. There should be at least two screens:

* A screen that allows the user to log in.
* A screen that shows the user their currently owned cards. For each card, the following
information should be displayed: Value in AUD, Voucher number, PIN, Product name
and description. The user should be able to “archive” the card which disables/hides it in
the UI.

### Requirements
The application should allow the customer to:

1. Log in (create an account & reset password are out of scope).
2. View a list of currently owned cards.
3. Archive any card. Archived cards should be clearly identified.

### Considerations

* Use any python library you feel is appropriate.
* Use any javascript library or framework you feel appropriate.
* A pretty UI is nice but definitely not required -- we’re interested in your code more than
your design skills.
* Include the automated tests you consider appropriate.
* Include any relevant instructions for someone else to install and run the app.
* Include some initial fixture to populate the database (at least 2 users with a few cards
each).

Optional: for bonus points, use a REST API on the backend, and use AJAX calls on the
front end to update the UI dynamically.

When you’re done, please upload to a git repo and grant access to prezzee.com.au.


# Solution

### To install and run

    % git clone https://gitlab.com/johnmee/prezzee-test.git
    % python3 -m venv venv && source venv/bin/activate
    % pip install -r requirements.txt

    % python manage.py makemigrations cards
    % python manage.py migrate
    % python manage.py loaddata initial_data.yaml
    % python manage.py runserver

Visit: http://127.0.0.1:8000/

### Users and passwords

    jack/password
    jill/password
    admin/password
    
